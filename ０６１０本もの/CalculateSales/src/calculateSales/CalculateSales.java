package calculateSales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {
	public static void main(String[] args) {

		HashMap<String, String> map = new HashMap<>();
		HashMap<String, Long> map2 = new HashMap<>();

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if (str.matches("^[0-9]{8}.rcd$")) { //^	行の先頭8桁、末尾rcd
					return true;
				} else {
					return false;
				}
			}
		};
		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String data;
			while ((data = br.readLine()) != null) {

				String[] brunchsplit = data.split(",");
				map.put(brunchsplit[0], brunchsplit[1]);//支店名
				map2.put(brunchsplit[0], 0L);

				if (!brunchsplit[0].matches("^[0-9]{3}$") || brunchsplit.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}

			File[] files = new File(args[0]).listFiles(filter);

			for (int i = 0; i < files.length - 1; i++) {

				int Namber = Integer.parseInt(files[i].getName().substring(0, 8));
				int Namber2 = Integer.parseInt(files[i + 1].getName().substring(0, 8));
				if (Namber2 - Namber != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}

			for (int i = 0; i < files.length; i++) {
				br = new BufferedReader(new FileReader(files[i]));
				String data2 = br.readLine();//支店コード 読む
				String data3 = br.readLine();//売上金額　読む

				if (!map2.containsKey(data2)) {
					String fileNumber = files[i].getName();
					System.out.println(fileNumber + "の支店コードが不正です");
					return;
				}
				if (br.readLine() != null) {

					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;
				}
				long lon = Long.parseLong(data3);//Longに変換

				map2.put(data2, lon + map2.get(data2));

				if (data3.length() >= 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}
			//for(Entry<String, String> entry : map.entrySet())
			//System.out.println(entry.getKey() + ":" + entry.getValue() + ":" + map2.get(entry.getKey())+"円");

			////////////////////出力/////////////////////////////////

			File output = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(new BufferedWriter(bw));

			for (Entry<String, String> entry2 : map.entrySet()) {

				pw.println((entry2.getKey() + "," + entry2.getValue() + "," + map2.get(entry2.getKey()) + "円"));
			}
			pw.close();

		} catch (IOException b) {
			b.printStackTrace();//エラー表示アシスト
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if (br != null) {

				try {
					br.close();
				} catch (IOException b) {
				}
			}
		}
	}
}
//3. エラー処理
//支店定義ファイル
//エラーメッセージ「支店定義ファイルが存在しません」 OK
//「支店定義ファイルのフォーマットが不正です」OK
//エラーメッセージ「売上ファイル名が連番になっていません」OK
//エラーメッセージ「合計金額が10桁を超えました」OK
//エラーメッセージ「<該当ファイル名>の支店コードが不正です」OK
//エラーメッセージ「<該当ファイル名>のフォーマットが不正です」OK
//エラーメッセージは「予期せぬエラーが発生しました」

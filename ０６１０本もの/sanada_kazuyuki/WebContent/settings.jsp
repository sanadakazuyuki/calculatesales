<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<h1>ユーザー編集画面</h1>
	<hr>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.user_name}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>
            	<a href="signup.jsp">ユーザー新規登録画面</a>
				<br />

            <form action="settings" method="post"><br />
                <input name="user_id" value="${editUser.user_id}" id="user_id" type="hidden"/>
                <label for="user_name">名称</label>
                <input name="user_name" value="${editUser.user_name}" id="user_name"/>（名前はあなたの公開プロフィールに表示されます）<br />

                <label for="password">新パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <label for="Branch">支店</label>
                <input name="Branch" value="${editUser.branch}" id="Branch"/> <br />

                <label for="Department_office">部署・役所</label>
                 <input name="Department_office" value="${editUser.department_office}" id="Department_office"/> <br />

                <input type="submit" value="登録" /> <br />
                <a href="./">戻る</a>
            </form>

        </div>
    </body>
</html>
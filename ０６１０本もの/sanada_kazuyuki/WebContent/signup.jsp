<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー新規登録画面</title>
     <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
    <h1>ユーザー新規登録画面</h1>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                       <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br> <label for="user_name">名称</label> <input name="user_name" id="user_name" />
                 <br><label for="Branch">支店</label> <input name="Branch" id="Branch" /> <br />
                     <label for="password">パスワード</label> <input name="password" type="password" id="password" />
                      <br /> <label for="Department_office">部署・役職	</label> <input name="Department_office" id="Department_office" /> <br />


                <br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
        </div>
    </body>
</html>
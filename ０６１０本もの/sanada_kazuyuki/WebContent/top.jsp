<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>ホーム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="header"></div>
	<h1>掲示板システム</h1>
	<hr>
	<div align="left">
		<c:if test="${  empty loginUser }">
			<br />
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="messages">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
			<form action="login" method="post">
				<br /> <label for="accountOrUser_name"> ログインID</label> <input
					name="accountOrUser_name" id="accountOrUser_name" /> <br /> <label
					for="password">パスワード</label> <input name="password" type="password"
					id="password" /> <br /> <input type="submit" value="ログイン" /> <br />
			</form>
		</c:if>
		<c:if test="${ not empty loginUser }">
			<a href="Newpost.jsp">新規投稿画面</a>
			<br />
			<a href="signup.jsp">ユーザー新規登録画面</a>
			<br />
			<a href="settings">ユーザー管理画面</a>
			<br />
			<a href="logout">ログアウト</a>
			<br />
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="messages">
						<c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />

			<div class="profile">
				<div class="user_name">
					<h2>
						<c:out value="${loginUser.user_name}" />
					</h2>
				</div>
				<div class="user_id">
					<c:out value="${loginUser.branch}" />
				</div>
				<div class="user_id">
					<c:out value="${loginUser.description}" />
				</div>
			</div>
		</c:if>



		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="account-name">
			<c:if test="${ message.is_delete == 0 }">
						件名：
						<c:out value="${message.title}" />
						<br /> 名前：
						<c:out value="${message.user_name}" />
						<br /> カテゴリ：
						<c:out value="${message.category}" />
						<hr>
						<div class="text">
							<c:out value="${message.text}" />
						</div>
						<div class="date">
							<fmt:formatDate value="${message.createdDate}"
								pattern="yyyy/MM/dd HH:mm:ss" />
						</div>

					<form action="delete" method="post">
									<input type="hidden" name="is_delete"value="1" >
									<input type="hidden" name="id"value="${message.id}" >
										<input type="submit" value="投稿を削除">
		</form>

						<hr>

						<div class="comments">
							<c:forEach items="${comments}" var="comment">
								<c:if test="${comment.message_id == message.id && comment.is_delete == 0 }">
									<div class="comment">
										<hr>
										名前：
										<c:out value="${comment.user_name}" />
										<br /> コメント：
										<c:out value="${comment.comments}" />
									</div>
									<div class="date">
										<fmt:formatDate value="${comment.createdDate}"
											pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
									<c:if test="${loginUser.user_id == comment.user_id}">




									<form action="commentdelete" method="post">
									<input type="hidden" name='is_delete'value=1 >
									<input type="hidden" name='id'value="${comment.id }" >
										<input type="submit" value="コメントを削除">
									</form>
									</c:if>

						</c:if>

							</c:forEach>
						</div>
						<div class="form-area">


							<c:if test="${ isShowMessageForm }">
								<form action=comments method="post">
									コメント<br />
									<textarea name="comments" cols="10" rows="5" maxlength="500"
										class="tweet-box"></textarea>
									<br /> <input type="hidden" name="message_id"
										value="${message.id}" /> <input type="submit" value="投稿">（500文字まで）
								</form>
								<hr>
							</c:if>
						</div>
							</c:if>
					</div>
				</div>

			</c:forEach>
		</div>
	</div>
</body>
</html>

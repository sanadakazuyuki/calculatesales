<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>

  <title>新規登録画面</title>
   <link href="./css/style.css" rel="stylesheet" type="text/css">
</head>

<!-- 画面タイトル -->
<div class="imui-title">
  <h1>新規登録画面</h1>

</div>

    <h2>入力フォーム</h2>

			<c:if test="${  empty loginUser }">
				<form action="login" method="post">
					<br /> <label for="accountOrUser_id"> ログインID</label> <input
						name="accountOrUser_id" id="accountOrUser_id" /> <br /> <label
						for="password">パスワード</label> <input name="password"
						type="password" id="password" /> <br /> <input type="submit"
						value="ログイン" /> <br />
				</form>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<br />
				<a href="signup.jsp">ユーザー新規登録画面</a>
				<br />
				<a href="logout">ログアウト</a>
				<br />


				<div class="profile">
					<div class="user_name">
						<h2>
							<c:out value="${loginUser.user_name}" />
						</h2>
					</div>
					<div class="user_id">
						<c:out value="${loginUser.branch}" />
					</div>
					<div class="user_id">
						<c:out value="${loginUser.description}" />
					</div>
				</div>

<div class="form-area">

					<form action="newMessage" method="post">

					    <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

<br><label for="title">タイトル<br />

<textarea name="title" cols="100" rows="1" maxlength="30" class="tweet-box"></textarea></label>
<br><label for="Category">カテゴリ<br />
<textarea name="Category" cols="50" rows="1" maxlength="10" class="tweet-box"></textarea></label>
<br><label for="text">新規投稿<br />
<textarea name="message" cols="100" rows="5" maxlength="1000" class="tweet-box"></textarea></label>
						<br />
						<input type="submit" value="投稿">(1000文字まで）
					</form>
					</div>
				</c:if>

	</body>
</html>

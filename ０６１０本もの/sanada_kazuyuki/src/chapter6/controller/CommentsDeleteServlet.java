package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Comments;
import chapter6.service.CommentsDeleteService;



	@WebServlet(urlPatterns = { "/commentdelete" })
	public class CommentsDeleteServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doPost(HttpServletRequest request,
	        HttpServletResponse response) throws IOException, ServletException{

	            Comments isdelete = new Comments();
	            isdelete.setId(Integer.parseInt(request.getParameter("id")));
	            isdelete.setIs_delete(Integer.parseInt(request.getParameter("is_delete")));


	            new  CommentsDeleteService().register(isdelete);

	            response.sendRedirect("./");
	           }
	   }

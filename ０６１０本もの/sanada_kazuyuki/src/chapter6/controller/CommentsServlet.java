package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comments;
import chapter6.beans.User;
import chapter6.service.CommentsService;

@WebServlet(urlPatterns = { "/comments" })
public class CommentsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException{

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comments comments = new Comments();
            comments.setComments(request.getParameter("comments"));
            comments.setUser_id(user.getUser_id());
            comments.setMessage_id(Integer.parseInt(request.getParameter("message_id")));


            new CommentsService().register(comments);

            response.sendRedirect("./");
           } else {
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("top.jsp").forward(request, response);
        }
   }

    private boolean isValid(HttpServletRequest request, List<String> messages)
    {

        String comment = request.getParameter("comments");

        if (StringUtils.isEmpty(comment) == true) {
        	messages.add("メッセージを入力してください");
        	}
        if (messages.size() == 0) {
            return true;
        }else {
            return false;
        }
    }
}
package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.service.DeleteService;



	@WebServlet(urlPatterns = { "/delete" })
	public class DeleteServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doPost(HttpServletRequest request,
	        HttpServletResponse response) throws IOException, ServletException{

	            Message isdelete = new Message();
	            isdelete.setId(Integer.parseInt(request.getParameter("id")));
	            isdelete.setDelete(Integer.parseInt(request.getParameter("is_delete")));


	            new  DeleteService().register(isdelete);

	            response.sendRedirect("./");
	           }
	   }

package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
//import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setUser_name(request.getParameter("user_name"));
            user.setPassword(request.getParameter("password"));
            user.setBranch(request.getParameter("Branch"));
            user.setDepartment_office(request.getParameter("Department_office"));
          //  user.setDescription(request.getParameter("description"));
       //   user.setUser_Stop_revival (request.getParameter("user_Stop_revival"));


            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String user_name = request.getParameter("user_name");
        String password = request.getParameter("password");
        String Branch = request.getParameter("Branch");
        String Department_office = request.getParameter("Department_office");


        if (StringUtils.isEmpty(user_name) == true) {
            messages.add("名称を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(Branch) == true) {
            messages.add("パスワードを入力してください");
        }
        if (StringUtils.isEmpty(Department_office) == true) {
            messages.add("部署・役職を入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
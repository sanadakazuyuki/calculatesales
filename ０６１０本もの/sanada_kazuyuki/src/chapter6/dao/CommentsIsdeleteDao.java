package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comments;
import chapter6.exception.SQLRuntimeException;

public class CommentsIsdeleteDao {

    public void update(Connection connection, Comments comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE comments SET is_delete=? WHERE id=?");

            ps = connection.prepareStatement(sql.toString());


            ps.setInt(2, comment.getId());
            ps.setInt(1, comment.getIs_delete());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
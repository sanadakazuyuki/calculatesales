package chapter6.dao;


	import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comments;
import chapter6.exception.SQLRuntimeException;

	public class CommentsDao {

	    public void insert(Connection connection, Comments comments) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO comments ( ");
	            sql.append("comments");
	            sql.append(", created_date");
	            sql.append(", updated_date");
	            sql.append(", user_id");
	            sql.append(", message_id");
	            sql.append(", is_delete");
	            sql.append(") VALUES (");
	            sql.append(" ?"); //Comments
	            sql.append(", CURRENT_TIMESTAMP"); // created_date
	            sql.append(", CURRENT_TIMESTAMP"); // updated_date
	            sql.append(", ?"); // user_id
	            sql.append(", ?");//message_id
	            sql.append(", 0");
	            sql.append(")");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, comments.getComments());
	            ps.setInt(2, comments.getUser_id());
	            ps.setInt(3, comments.getMessage_id());

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	}
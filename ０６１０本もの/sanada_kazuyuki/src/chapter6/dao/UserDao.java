package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("user_name");
            sql.append(", Branch");
            sql.append(", password");
            sql.append(", Department_office");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // user_name
            sql.append(", ?"); // Branch
            sql.append(", ?"); // password
            sql.append(", ?"); // Department_office
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date

            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getUser_name());
            ps.setString(2, user.getBranch());
            ps.setString(3, user.getPassword());
            ps.setString(4, user.getDepartment_office());
            System.out.println(ps.toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String accountOruser_name,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (user_id = ? OR user_name = ?) AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, accountOruser_name);
            ps.setString(2, accountOruser_name);
            ps.setString(3, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int user_id = rs.getInt("user_id");
                String user_name = rs.getString("user_name");
                String Branch = rs.getString("Branch");
                String password = rs.getString("password");
                String Department_office = rs.getString("Department_office");
         //       String description = rs.getString("description");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setUser_id(user_id);
                user.setUser_name(user_name);
                user.setBranch(Branch);
                user.setPassword(password);
                user.setDepartment_office(Department_office);
             //   user.setDescription(description);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int user_id) {
    	System.out.println(user_id);
    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE user_id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, user_id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }




    public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" user_name = ?");
			sql.append(", Branch = ?");
			sql.append(", password = ?");
			sql.append(", department_office = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" user_id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getUser_name());
			ps.setString(2, user.getBranch());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getDepartment_office());
			ps.setInt(5, user.getUser_id());


			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

	}

package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComments;
import chapter6.exception.SQLRuntimeException;

public class UserCommentsDao {

    public List<UserComments> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.comments as comments, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.user_name as user_name, ");
            sql.append("comments.message_id as message_id, ");
            sql.append("comments.is_delete as is_delete, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComments> ret = toUserCommentsList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComments> toUserCommentsList(ResultSet rs)
            throws SQLException {

        List<UserComments> ret = new ArrayList<UserComments>();
        try {
            while (rs.next()) {
                String comments = rs.getString("comments");
                int user_id = rs.getInt("user_id");
                int message_id = rs.getInt("message_id");
                String user_name  = rs.getString("user_name");
                int id = rs.getInt("id");
                int is_delete = rs.getInt("is_delete");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComments Comments = new UserComments();
                Comments.setComments(comments);
                Comments.setUser_name(user_name);
                Comments.setMessage_id(message_id);
                Comments.setId(id);
                Comments.setUser_id(user_id);
                Comments.setCreatedDate(createdDate);
                Comments.setIs_delete(is_delete);

                ret.add(Comments);

            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
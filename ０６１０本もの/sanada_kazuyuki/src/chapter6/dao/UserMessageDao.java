package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.Category as Category, ");
            sql.append("messages.text as text, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("users.Branch as branch, ");
            sql.append("users.user_name as user_name, ");
            sql.append("messages.created_date as created_date, ");
            sql.append("messages.is_delete as is_delete ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.user_id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                String user_name = rs.getString("user_name");
                String title = rs.getString("title");
                String Category = rs.getString("Category");
                String Branch = rs.getString("branch");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                int is_delete = rs.getInt("is_delete");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserMessage message = new UserMessage();
                message.setUser_name(user_name);
                message.setTitle(title);
                message.setCategory(Category);
                message.setBranch(Branch);
                message.setId(id);
                message.setUser_id(user_id);
                message.setIs_delete(is_delete);
                message.setText(text);
                message.setCreatedDate(createdDate);


                ret.add(message);

            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
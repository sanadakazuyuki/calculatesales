package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Comments;
import chapter6.beans.UserComments;
import chapter6.dao.CommentsDao;
import chapter6.dao.UserCommentsDao;


public class CommentsService {

    public void register(Comments comments) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentsDao commentsDao = new CommentsDao();
            commentsDao.insert(connection, comments);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserComments> getComments() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserCommentsDao commentsDao = new UserCommentsDao();
    		List<UserComments> ret = commentsDao.getUserComments(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}
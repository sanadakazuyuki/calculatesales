package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import chapter6.beans.Comments;
import chapter6.dao.CommentsIsdeleteDao;


public class CommentsDeleteService {

    public void register(Comments isdelete) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentsIsdeleteDao commentsIsdeleteDao = new CommentsIsdeleteDao();
            commentsIsdeleteDao.update(connection, isdelete);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
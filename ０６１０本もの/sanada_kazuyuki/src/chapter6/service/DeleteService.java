package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;

import chapter6.beans.Message;
import chapter6.dao.DeleteDao;


public class DeleteService {

    public void register(Message isdelete) {

        Connection connection = null;
        try {
            connection = getConnection();

            DeleteDao DeleteDao = new DeleteDao();
            DeleteDao.update(connection, isdelete);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}